FROM ubuntu:latest

#
# Add Mono Repository
#
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF && \
  echo "deb http://download.mono-project.com/repo/ubuntu xenial main" | tee /etc/apt/sources.list.d/mono-official.list

#
# Install Prerequisites
#
RUN apt-get update && \
  apt-get install -y \
  ca-certificates \
  mono-devel \
  openjdk-9-jdk-headless \
  rbenv \
  curl \
  git \
  libssl-dev \
  libreadline-dev \
  zlib1g-dev

#
# Install Ruby
#
RUN mkdir -p "$(rbenv root)"/plugins && \
  git clone https://github.com/rbenv/ruby-build.git "$(rbenv root)"/plugins/ruby-build

RUN rbenv install 2.4.2 && \
  rbenv local 2.4.2 && \
  eval "$(rbenv init -)" && \
  gem install bundler gauge-ruby

#
# Install Python
#
RUN curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
ENV PATH="/root/.pyenv/bin:$PATH"
RUN pyenv install 3.6.3 && \
  pyenv local 3.6.3 && \
  eval "$(pyenv init -)" && \
  pip install colorama getgauge

#
# Install Gauge
#
RUN curl -SsL https://downloads.getgauge.io/stable | sh

RUN gauge install java && \
  gauge install ruby &&  \
  gauge install csharp && \
  gauge install python && \
  gauge install html-report && \
  gauge install xml-report && \
  gauge install spectacle

#
# Setup Entrypoint
#
COPY gauge.sh gauge.sh
RUN chmod ugo+x gauge.sh
ENTRYPOINT ["/gauge.sh"]
CMD ["gem", "list"]
