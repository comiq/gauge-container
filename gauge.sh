#!/bin/bash

rbenv local 2.4.2
eval "$(rbenv init -)"

pyenv local 3.6.3
eval "$(pyenv init -)"

exec $@

